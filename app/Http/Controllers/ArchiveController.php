<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.10.17
 * Time: 15:32
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class ArchiveController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show tasks in archive
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        $user = Auth::user();
        $tasks = $user->finishedTasks;
        return view('archive/view', [
            'tasks' => $tasks,
        ]);
    }
}