<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Services\FilterService;
use Illuminate\Http\Request;
use Carbon\Carbon;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, FilterService $filterService)
    {
        $user = Auth::user();
        $projects = $user->projects;
        $input = $request->input('term');
        $data = $filterService->getTasksByTerm($projects, $input);
        if (empty($data)) {
            return redirect()->back();
        }
        return view('homepage', [
            'projects' => $projects,
            'tasks' => $data['tasks'],
            'term' => $data['term'],
            'date' => $data['date'],
        ]);
    }
}
