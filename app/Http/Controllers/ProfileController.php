<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.10.17
 * Time: 13:56
 */

namespace App\Http\Controllers;


use App\Http\Requests\ProfileUpdateRequest;
use App\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Action that shows user profile
     */
    public function view()
    {
        $user = Auth::user();
        $avatar = $user->images()->avatar()->get();
        $date = Carbon::today();
        return view('profile/view', [
            'user' => $user,
            'date' => $date,
            'avatar' => $avatar->first(),
        ]);

    }

    /**
     * Action for profile update
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update()
    {
        $user = Auth::user();
        return view('profile/update', [
            'user' => $user,
        ]);
    }

    /**
     * Store update info
     * @param Request $request
     */
    public function store(ProfileUpdateRequest $request)
    {
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->save();
        if ($request->file('avatar')) {
            $path = $request->file('avatar')->store('public/images');
            $name = $request->file('avatar')->hashName();
            $prevAvatar = $user->images()->avatar()->get()->first();
            if ($prevAvatar) {
                $prevAvatar->delete();
            }
            $image = new Image();
            $image->name = $name;
            $image->path = $path;
            $image->user_id = $user->id;
            $image->type = 0;
            $image->save();
            return redirect()->route('profile_view');
        }
        return redirect()->route('profile_view');
    }

    public function avatarDelete()
    {
        $user = Auth::user();
        $avatar = $user->images()->avatar()->get()->first();
        if($avatar) {
            $avatar->delete();
        }
        return redirect()->route('profile_view');

    }
}