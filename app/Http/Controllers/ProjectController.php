<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectForm;
use App\Project;
use App\Services\FilterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * @var
     */
    protected $filterService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
        $this->middleware('auth');
    }

    /**
     * Shows project
     *
     * @param $id
     * @param null $term
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function view(Request $request, $id)
    {
        $user = Auth::user();
        $project = app('project.repo')->find($id);
        $projects = $user->projects;
        $input = $request->input('term');
        $data = $this->filterService->getTasksByTerm($project, $input);
        if (empty($data)) {
            return redirect()->back();
        }
        return view('project/view', [
            'project' => $project,
            'projects' => $projects,
            'tasks' => $data['tasks'],
            'term' => $data['term'],
            'date' => $data['date'],
        ]);

    }

    /**
     * Create the new project.
     *
     * @return view
     */
    public function create()
    {
        $user = Auth::user();
        return view('project/create');
    }

    /**
     * Project updating action
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update($id)
    {
        $project = app('project.repo')->find($id);
        return view('project/update', ['project' => $project]);
    }

    /**
     * @param ProjectForm $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProjectForm $request)
    {
        $user = Auth::user();
        $input = $request->all();
        if (isset($input['project_id'])) {
            $project = app('project.repo')->find($input['project_id']);
        } else {
            $project = new Project();
            $project->user_id = $user->id;
        }
        $project->name = $input['name'];
        $project->save();
        return redirect()->route('project_view', ['id' => $project->id]);
    }

    /**
     * Project deleting action
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function delete($id)
    {
        app('project.repo')->delete($id);
        return redirect()->route('home');
    }

}