<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskUpdateRequest;
use App\Services\FilterService;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests\TaskForm;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TaskController extends Controller
{

    /**
     * @var
     */
    protected $filterService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        $projects = $user->projects;
        $project = null;
        $projectName = $request->input('project');
        if (isset($projectName)) {
            $project = app('project.repo')->findBy('name', $projectName);
        }
        return view('task/create', [
            'project' => $project,
            'projects' => $projects,
            'date' => Carbon::tomorrow(),
        ]);
    }

    /**
     * Task updating action
     *
     * @param $id
     * @param TaskForm $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update($id, TaskUpdateRequest $request)
    {
        $user = Auth::user();
        $task = app('task.repo')->find($id);
        $projects = $user->projects;
        $input = $request->all();
        if ($input) {
            unset($input['_token']);
            unset($input['task_id']);
            app('task.repo')->update($input, $task->id);
            return redirect()->route('project_view', ['id' => $task->project_id]);
        }
        return view('task/update', [
            'task' => $task,
            'projects' => $projects,
        ]);
    }

    /**
     * Task deleting action
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function delete($id)
    {
        app('task.repo')->delete($id);
        return redirect()->back();
    }

    /**
     * Show the task
     *
     * @param $id
     * @return view
     */
    public function view($id)
    {
        $task = app('task.repo')->find($id);
        return view('task/view', [
            'task' => $task,
        ]);
    }

    public function store(TaskForm $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $task = new Task();
        $task->state = 0;
        $task->user_id = $user->id;
        $task->name = $input['name'];
        $task->project_id = $input['project_id'];
        $task->date = $input['date'];
        $task->priority = $input['priority'];
        $task->save();
        return redirect()->route('project_view', ['id' => $task->project_id]);
    }
}