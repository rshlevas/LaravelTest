<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.10.17
 * Time: 11:40
 */

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TestController extends Controller
{
    public function index()
    {
        $url1 = 'http://football.ua';
        $url2 = 'http://dovzhenko.zp.ua/';
        $url3 = 'http://dovzhenko.zp.ua/kinoafisha/';
        $client = new Client();
        $response = $client->request('GET', $url1);
        $data = $response->getBody()->getContents();
        $imageSrc = $this->getImgSorce($data);
        dd($imageSrc);
    }

    protected function getImgSorce(string $data) : array
    {
        $matches = [];
        $reg = '/<img(?:\\s[^<>]*?)?\\bsrc\\s*=\\s*(?|("[^"]*")|(\'[^\']*\')|([^<>\'"\\s]*))[^<>]*>/';
        preg_match_all($reg, $data, $matches);
        $urls = [];
        $urls = $matches[1];
        $urlUnique = array_values(array_unique($urls));
        return $urlUnique;
    }
}