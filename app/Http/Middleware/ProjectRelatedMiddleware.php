<?php

namespace App\Http\Middleware;

use App\Services\FilterService;
use Closure;
use Illuminate\Support\Facades\Auth;

class ProjectRelatedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $filterService = new FilterService();

        if (!$filterService->userRelationsChecker($request->route('id'), 'projects', $user)) {
            return redirect('home');
        }
        return $next($request);
    }
}
