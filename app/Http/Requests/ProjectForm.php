<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProjectForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        $rules = [
            'name' => 'required|unique:projects,name,NULL,id,user_id,' . $user->id,
        ];
        $input = $this->all();
        if (isset($input['project_id'])) {
            $project = app('project.repo')->find($input['project_id']);
            if ($input['name'] != $project->name) {
                return $rules;
            } else {
                return [
                    'name' => 'required',
                ];
            }
        }
        return $rules;
    }
}
