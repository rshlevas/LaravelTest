<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class TaskUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date' => 'required|date|date_format:Y-m-d',
        ];
        $input = $this->all();
        if ($input) {
            $task = app('task.repo')->find($input['task_id']);
            if ($input['name'] != $task->name ||
                ($task->name == $input['name'] && $task->project_id != $input['project_id'])) {
                $rules['name'] = 'required|unique:tasks,name,NULL,id,project_id,' . $input['project_id'];
            }
            return $rules;
        } else {
            return [];
        }
    }
}
