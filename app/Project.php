<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id',
    ];

    /**
     * Get the tasks for the user project.
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    /**
     * Get the tasks for the user project.
     */
    public function activeTasks()
    {
        return $this->hasMany('App\Task')->where('state', 0);
    }

    /**
     * Get the user that owns the project.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
