<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\FilterService;

class FilterServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('filter.service', function () {
            return new FilterService();
        });
    }
}
