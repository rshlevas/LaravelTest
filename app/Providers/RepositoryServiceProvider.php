<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.10.17
 * Time: 15:34
 */

namespace App\Providers;


use App\Task;
use Illuminate\Support\ServiceProvider;
use App\Repositories\ProjectRepository;
use App\Repositories\TaskRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('task.repo', function () {
            return new TaskRepository();
        });

        $this->app->singleton('project.repo', function () {
            return new ProjectRepository();
        });
    }
}