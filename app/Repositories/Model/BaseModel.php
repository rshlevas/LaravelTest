<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.10.17
 * Time: 11:12
 */

namespace App\Repositories\Model;


abstract class BaseModel implements ModelInterface
{
    public $id;

    protected $table;

    protected $conn;

    public function __construct()
    {
        $this->setTable();
        $this->conn = $this->getConnection();
    }

    protected abstract function setTable();

    protected function getTable(){
        return $this->table;
    }

    protected function getConnection() {
        $connInfo = $this->getConnAttr();
        return new \PDO(
            $connInfo['conn'].
            ':host='.$connInfo['hostname'].
            ';dbname='.$connInfo['db'],
            $connInfo['user'],
            $connInfo['pass']);

    }

    private function getConnAttr(): array {
        return [
            'conn' => env("DB_CONNECTION"),
            'hostname' => env("DB_HOST"),
            'db' => env("DB_DATABASE"),
            'user' => env("DB_USERNAME"),
            'pass' => env("DB_PASSWORD")
        ];
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function get($columns = null) {

    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*')) {

    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {

    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data) {

    }

    /**
     *
     */
    public function save() {
        $attrs = get_object_vars($this);
        $id = $attrs['id'];
        unset($attrs['id']);
        unset($attrs['table']);
        unset($attrs['conn']);
        $queries = [];
        foreach($attrs as $attr => $val) {
            $queries[] = $attr. ' = \''. $val . '\'';
        }
        $updateString = implode(', ', $queries);
        $sql = 'UPDATE '. $this->table . ' SET '. $updateString . ' WHERE id = ?';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $id,  \PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id) {
        $stmt = $this->conn->prepare("DELETE from $this->table WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return true;
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*')) {
        $stmt = $this->conn->prepare("SELECT * from $this->table WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchObject(get_class($this));
    }

}