<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.10.17
 * Time: 11:14
 */

namespace App\Repositories\Model;


interface ModelInterface
{
    /**
     * @param array $columns
     * @return mixed
     */
    public function get($columns);

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*'));

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'));

    /**
     * @return boolean
     */
    public function save();
}