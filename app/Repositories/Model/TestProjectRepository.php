<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.10.17
 * Time: 12:29
 */

namespace App\Repositories\Model;

use App\Repositories\TestProject;
use App\Repositories\BaseRepository\Repository;


class TestProjectRepository extends Repository
{
    /**
     * TaskRepository constructor.
     * @return void
     */
    public function __construct()
    {
        $this->model = $this->model();
    }

    /**
     * Get model name
     * @return mixed
     */
    protected function model()
    {
        return new TestProject();
    }
}