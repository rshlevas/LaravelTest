<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.10.17
 * Time: 11:18
 */

namespace App\Repositories;

use App\Project;
use App\Repositories\BaseRepository\Repository;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProjectRepository
 * @package App\Repositories
 */

class ProjectRepository extends Repository
{
    /**
     * TaskRepository constructor.
     * @return void
     */
    public function __construct()
    {
        $this->model = $this->model();
    }

    /**
     * Get model name
     * @return mixed
     */
    protected function model()
    {
        return new Project();
    }
}