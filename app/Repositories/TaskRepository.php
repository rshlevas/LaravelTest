<?php

namespace App\Repositories;

use App\Task;
use App\Repositories\BaseRepository\Repository;
use Illuminate\Support\Facades\Auth;

/**
 * Class TaskRepository
 * @package App\Repositories
 */

class TaskRepository extends Repository
{
    /**
     * TaskRepository constructor.
     * @return void
     */
    public function __construct()
    {
        $this->model = $this->model();
    }

    /**
     * Get model name
     * @return mixed
     */
    protected function model() {
        return new Task();
    }

    /**
     * @param $attribute
     * @param $value
     * @return mixed
     */
    public function getUnfinishedByAttribute($attribute, $value)
    {
        return $this->model->where($attribute, $value)
            ->where('state', 0)
            ->orderBy('date', 'asc')
            ->get();
    }

    /**
     * @param $attribute
     * @param $value
     * @return mixed
     */
    public function getUnfinishedByAttrForPeriod($attribute, $value, $date)
    {
        return $this->model->where($attribute, $value)
            ->where('state', 0)->where('date', '<=', $date)
            ->orderBy('date', 'asc')
            ->get();
    }

    /**
     * @param $attribute
     * @param $value
     * @return mixed
     */
    public function getFinishedTasksByAttribute($attribute, $value)
    {
        return $this->model->where($attribute, $value)->where('state', 1)->get();
    }
}