<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.10.17
 * Time: 11:37
 */

namespace App\Repositories;

use App\Repositories\Model\BaseModel;


class TestProject extends BaseModel
{

    protected function setTable() {
        return $this->table = 'projects';
    }
}