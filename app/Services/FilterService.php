<?php


namespace App\Services;

use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;


class FilterService
{
    /**
     * Return collection of tasks related to single project or
     * collection of projects.
     *
     * @param $projects
     * @param string|null $term
     * @return array
     */
    public function getTasksByTerm($projects, string $term = null): array
    {
        if ($projects instanceof Project) {
            return $this->getTasksSingleProject($projects, $term);
        } elseif ($projects instanceof Collection) {
            return $this->getTasksProjectsCollect($projects, $term);
        } else {
            return [];
        }

    }

    /**
     * Return task collection of the single project
     *
     * @param Project $project
     * @param string|null $term
     * @return array
     */
    protected function getTasksSingleProject(Project $project, string $term = null): array
    {
        $data = [];
        $data['date'] = Carbon::today();

        if($term == null) {
            $data['tasks'] = $this->getAllActiveTasks($project);
            $data['term'] = 'General';
            return $data;
        } elseif ($term == 'today') {
            $data['tasks'] = $this->getActiveTasksByTerm($project, $data['date']);
            $data['term'] = 'Today';
            return $data;
        } elseif ($term == 'week') {
            $date = Carbon::today();
            $date->day += 7;
            $data['tasks'] = $this->getActiveTasksByTerm($project, $date);
            $data['term'] = '7 days';
            return $data;
        } else {
            return [];
        }
    }

    /**
     * Return task collection related to the projects collection
     *
     * @param Collection $projects
     * @param string|null $term
     * @return array
     */
    protected function getTasksProjectsCollect(Collection $projects, string $term = null): array
    {
        if (!$projects->first()) {
            return $this->emptyProjectCollection($term);
        } elseif (!$projects->first() instanceof Project) {
            return [];
        }
        $data = [
            'date' => Carbon::today(),
        ];

        if($term == null) {
            $data['term'] = 'General';
            foreach ($projects as $project) {
                $data['tasks'][] = $this->getAllActiveTasks($project);
            }
            $data['tasks'] = $this->getSortedTasks($data['tasks']);
            return $data;
        } elseif ($term == 'today') {
            $data['term'] = 'Today';
            foreach ($projects as $project) {
                $data['tasks'][] = $this->getActiveTasksByTerm($project, $data['date']);
            }
            $data['tasks'] = $this->getSortedTasks($data['tasks']);
            return $data;
        } elseif ($term == 'week') {
            $data['term'] = '7 days';
            $date = Carbon::today();
            $date->day += 7;
            foreach ($projects as $project) {
                $data['tasks'][] = $this->getActiveTasksByTerm($project, $date);
            }
            $data['tasks'] = $this->getSortedTasks($data['tasks']);
            return $data;
        } else {
            return [];
        }
    }


    /**
     * Get all active tasks related to project
     *
     * @param Project $project
     * @return mixed
     */
    protected function getAllActiveTasks (Project $project)
    {
        return $project->activeTasks()->orderByDate()->get();
    }

    /**
     * Get active tasks related to project up to given date
     *
     * @param Project $project
     * @param Carbon $date
     * @return mixed
     */
    protected function getActiveTasksByTerm (Project $project, Carbon $date)
    {
        return $project->activeTasks()->term($date)->orderByDate()->get();
    }

    /**
     * Sorted tasks collection by date asc
     *
     * @param array $tasks
     * @return array
     */
    protected function getSortedTasks (array $tasks): array
    {
        $sortedTasks = [];
        foreach ($tasks as $collection) {
            foreach ($collection as $item) {
                $sortedTasks[] = $item;
            }
        }
        uasort($sortedTasks, function($a, $b) {
            if ($a->date == $b->date) {
                return 0;
            }
            return ($a->date < $b->date) ? -1 : 1;
        });
        return $sortedTasks;
    }

    /**
     * @param $term
     * @return array
     */
    protected function emptyProjectCollection(string $term = null): array
    {
        $data = [
            'date' => Carbon::today(),
            'tasks' => [],
        ];
        switch($term) {
            case null:
                $data['term'] = 'General';
                break;
            case 'today':
                $data['term'] = 'Today';
                break;
            case 'week':
                $data['term'] = '7 days';
                break;
            default:
                return [];
        }
        return $data;

    }

    /**
     * Checked of existence of objects and their relations with authorised user
     *
     * @param int $id
     * @param string $type
     * @return bool
     */
    public function userRelationsChecker(int $id, string $type, User $user): bool
    {
        if (method_exists($user, $type)) {
            return $this->checker($id, $user, $type);
        } else {
            throw new \Exception('There is no such method in User Model');
        }

    }

    /**
     * Checks relations
     *
     * @param int $id
     * @param User $user
     * @param string $type
     * @return bool
     */
    protected function checker(int $id, User $user, string $type)
    {
        $collection = $user->{$type};
        foreach ($collection as $item) {
            if ($item->id == $id) {
                return true;
            }
        }
        return false;
    }

}