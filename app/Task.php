<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Task extends Model
{
    /**
     * Get the project that owns the task.
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    /**
     * Get the user that owns the task.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get task name and project name attribute
     */
    public function getTaskProjectAttribute()
    {
        return $this->name .'-'. $this->project->name;
    }


    /**
     * Get tasks for todays date
     *
     * @param $query
     * @return mixed
     */
    public function scopeTerm($query, Carbon $date)
    {
        return $query->where('date', '<=', $date->toDateString());
    }

    /**
     * Get tasks ordered by date
     */
    public function scopeOrderByDate($query)
    {
        return $query->orderBY('date', 'asc');
    }
}
