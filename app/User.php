<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the project of the User.
     */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    /**
     * Get tasks of the User.
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    /**
     * Get images of the User.
     */
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    /**
     * Get finished tasks of user
     */
    public function finishedTasks()
    {
        return $this->hasMany('App\Task')->where('state', 1);
    }
}
