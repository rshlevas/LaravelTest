var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');

gulp.task('sass', function(){
    gulp.src('resources/assets/sass/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/css/'))
});

gulp.task('watch', function(){
   return gulp.watch('resources/assets/sass/**/*.scss', ['sass']);
});