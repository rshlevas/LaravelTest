@extends('layouts.app')

@section('title', 'Archive...')

@section('content')
    <div class="container clearfix">
        <div class="row clearfix">
            <div class="content">
                <div class="task-archive-container">
                    <div class="task-header"
                    ><h2>Archive </h2>
                    </div>
                    <div class="btn-back"><a href="{{ route('home')  }}">Back</a></div>
                    <div class="task-lits-body">
                        @foreach ($tasks as $task)
                            <div class="task-item">
                                <div class="task-name"><a
                                            href="{{ route('task_view', ['id' => $task->id]) }}">{{  $task->name }}</a>
                                </div>
                                <div class="task-category"><em>Project:</em>  <strong>{{ $task->project->name }}</strong></div>
                                <div class="task-content">
                                    <div class="task-date">Finished at:
                                        <br>
                                        {{ $task->updated_at }}</div>
                                    <div class="task-links">
                                        <a href="{{ route('task_update', ['id' => $task->id]) }}">Edit</a>
                                        <a href="{{ route('task_delete', ['id' => $task->id]) }}">Delete</a>
                                    </div>

                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection