@extends('layouts.app')

@section('title', 'Login')

@section('content')
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="task-lits-body">
                    <div class="form-title"><h3>Login</h3></div>

                    <div class="form-body">
                        @if ($errors->any())
                            <div class="form-group">
                                @foreach ($errors->all() as $error)
                                    <div class="for-input help-block">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif

                        <form class="form" method="POST" action="{{ route('login') }}" novalidate>
                            {{ csrf_field() }}

                            <div class="form-group">
                                <div class="label"><label for="email" class="col-md-4 control-label">E-Mail Address</label></div>
                                <div class="for-input">
                                    <input id="email" type="email" class="form-control" name="email"
                                           placeholder="Your email"
                                           value="{{ old('email') }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="label"><label for="password" class="col-md-4 control-label">Password</label></div>
                                <div class="for-input">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                   </div>
                            </div>

                            <div class="form-group">
                                <div class="for-input">
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="checkbox">Remember Me</label>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="for-input">
                                    <button type="submit" class="submit">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
