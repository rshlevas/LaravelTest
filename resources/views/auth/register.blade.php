@extends('layouts.app')

@section('title', 'Register')

@section('content')
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="task-lits-body">
                    <div class="form-title"><h3>Register</h3></div>

                    <div class="form-body">
                        @if ($errors->any())
                            <div class="form-group">
                                @foreach ($errors->all() as $error)
                                    <div class="for-input help-block">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{ route('register') }}" novalidate>
                            {{ csrf_field() }}

                            <div class="form-group">
                                <div class="label"><label for="name" class="col-md-4 control-label">Name</label></div>
                                <div class="for-input">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                           placeholder="John Smith"
                                           required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="label"><label for="email" class="col-md-4 control-label">E-Mail Address</label></div>
                                <div class="for-input">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                           placeholder="you@example.com"
                                           required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="label"><label for="password" class="col-md-4 control-label">Password</label></div>
                                <div class="for-input">
                                    <input id="password" type="password" class="form-control" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label"><label for="password-confirm" class="col-md-4 control-label">Confirm Password</label></div>
                                <div class="for-input">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="for-input">
                                    <button type="submit" class="submit">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
