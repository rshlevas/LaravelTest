@extends('layouts.app')

@section('title', 'Homepage')

@section('content')
    <div class="container clearfix">
        <div class="row clearfix">

            @include('includes.sidebar')

            @include('includes.task_list')
        </div>
    </div>
@endsection