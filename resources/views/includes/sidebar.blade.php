<div id="sidebar" class="left-sidebar">
    <nav id="sidebar-nav">
        <ul class="sidebar-list">
            <li><h4>Terms</h4></li>
            @if (isset($project))
                <li @if ($term == 'General') class="active" @endif>
                    <a href="{{ route('project_view', ['id' => $project->id]) }}">All</a>
                </li>
                <li @if ($term == '7 days') class="active" @endif>
                    <a href="{{ route('project_view', ['id' => $project->id, 'term' => 'week']) }}">7 Days</a>
                </li>
                <li @if ($term == 'Today') class="active" @endif>
                    <a href="{{ route('project_view', ['id' => $project->id, 'term' => 'today']) }}">Today</a>
                </li>
            @else
                <li @if ($term == 'General') class="active" @endif>
                    <a href="{{ route('home') }}">All</a>
                </li>
                <li @if ($term == '7 days') class="active" @endif>
                    <a href="{{ route('home', ['term' => 'week']) }}">7 Days</a>
                </li>
                <li @if ($term == 'Today') class="active" @endif>
                    <a href="{{ route('home', ['term' => 'today']) }}">Today</a>
                </li>
            @endif
            <li><h4>Projects</h4></li>
            @foreach ($projects as $projectItem)
                <li
                    @if (isset($project) && $project->id == $projectItem->id)
                        class="active"
                    @endif
                >
                    <a href="{{ route('project_view', ['id' => $projectItem->id]) }}">{{ $projectItem->name }}</a>
                    <ul class="sub-menu">
                        <li><a href="{{ route('project_update', ['id' => $projectItem->id]) }}">Edit</a></li>
                        <li><a href="{{ route('project_delete', ['id' => $projectItem->id]) }}">Delete</a></li>
                    </ul>

                </li>
            @endforeach
            <li class="active"><a href="{{ route('project_create') }}"><- Add Project</a></li>
        </ul>
    </nav>
</div>