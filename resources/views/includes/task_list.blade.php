<div class="content">
    <div class="task-container">
        <div class="task-header"
            ><h2>
                {{ $term }}:
                @if ($term == '7 days')
                    {{ $date->toDateString() }} -
                    @php $week = \Carbon\Carbon::today();
                        $week->day += 7;
                    @endphp
                    {{  $week->toDateString()}}
                @elseif ($term == 'Today')
                    {{ $date->toDateString() }}
                @endif
            </h2>
        </div>
        <div class="task-header">
            <div class="btn-back">
                @if (isset($project))
                    <a href="{{ route('task_create', ['project' => $project->name])  }}">Add Task</a>
                @else
                    <a href="{{ route('task_create')  }}">Add Task</a>
                @endif
            </div>
        </div>

        <!-- If user hasn't any tasks or tasks for current project -->
        @if ((is_array($tasks) && empty($tasks)) || (!is_array($tasks) && !$tasks->first()))
            <div class="task-header"><h4>There are no tasks</h4></div>

        <!-- If user has tasks-->
        @else
            <div class="task-lits-body">
                @foreach ($tasks as $task)
                    <div class="task-item">
                        <div class="task-name"><a
                                    href="{{ route('task_view', ['id' => $task->id]) }}">{{  $task->name }}</a>
                        </div>
                        <div class="task-category">{{ $task->project->name }}</div>
                        <div class="task-priority
                                @if ($task->date >= $date->toDateString())
                                priority-{{ $task->priority }}
                        @endif
                                ">
                        </div>

                        <div class="task-content">
                            <div class="task-date">Up to: {{ $task->date }}</div>
                            <div class="task-links">
                                <a href="{{ route('task_update', ['id' => $task->id]) }}">Edit</a>
                                <a href="{{ route('task_delete', ['id' => $task->id]) }}">Delete</a>
                            </div>

                        </div>
                    </div>
                @endforeach

            @endif
        </div>
    </div>
</div>
