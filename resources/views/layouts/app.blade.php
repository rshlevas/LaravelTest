<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="clearfix">
            <div class="header">
                <div class="container">
                <div class="header-nav-bar navbar-left">
                    <ul>
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <input type="checkbox" id="toggle-menu">
                        @guest
                        @else
                        <li class="projects-menu">
                            <a href="#"><label class="sub-menu-link" for="toggle-menu">Projects</label></a>
                            @if (isset($projects) && isset($term))
                            <nav class="projects" role='navigation'>
                                <ul class="project-list">
                                    <li>Terms</li>
                                    @if (isset($project))
                                        <li @if ($term == 'General') class="active" @endif>
                                            <a href="{{ route('project_view', ['id' => $project->id]) }}">All</a>
                                        </li>
                                        <li @if ($term == '7 days') class="active" @endif>
                                            <a href="{{ route('project_view', ['id' => $project->id, 'term' => 'week']) }}">7 Days</a>
                                        </li>
                                        <li @if ($term == 'Today') class="active" @endif>
                                            <a href="{{ route('project_view', ['id' => $project->id, 'term' => 'today']) }}">Today</a>
                                        </li>
                                    @else
                                        <li @if ($term == 'General') class="active" @endif>
                                            <a href="{{ route('home') }}">All</a>
                                        </li>
                                        <li @if ($term == '7 days') class="active" @endif>
                                            <a href="{{ route('home', ['term' => 'week']) }}">7 Days</a>
                                        </li>
                                        <li @if ($term == 'Today') class="active" @endif>
                                            <a href="{{ route('home', ['term' => 'today']) }}">Today</a>
                                        </li>
                                    @endif
                                    <li>Projects</li>
                                    @foreach ($projects as $projectItem)
                                        <li
                                                @if (isset($project) && $project->id == $projectItem->id)
                                                class="active"
                                                @endif
                                        >
                                            <a class="project-item" href="{{ route('project_view', ['id' => $projectItem->id]) }}">{{ $projectItem->name }}</a>
                                            <a class="submenu-buttons first" href="{{ route('project_update', ['id' => $projectItem->id]) }}">Edit</a>
                                            <a class="submenu-buttons second" href="{{ route('project_delete', ['id' => $projectItem->id]) }}">Delete</a>
                                        </li>
                                    @endforeach
                                    <li class="active"><a href="{{ route('project_create') }}"><- Add Project</a></li>
                                </ul>
                            </nav>
                          @endif
                        </li>
                        @endguest
                    </ul>
                </div>
                <div class="header-nav-bar navbar-right">
                    <ul>
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li><a href="{{ route('profile_view') }}">{{ Auth::user()->name }}</a></li>
                            <li><a href="{{ route('archive_view') }}">Archive</a></li>
                            <li><a href="{{ route('logout') }}">Logout</a></li>
                        @endguest
                    </ul>
                </div>
                </div>
            </div>


        @yield('content')
    </div>

</body>
</html>
