@extends('layouts.app')

@section('title', 'Profile Update...')

@section('content')
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="task-lits-body">
                    <div class="form-title"><h3>Project Updating</h3></div>

                    <div class="form-body">
                        @if ($errors->any())
                            <div class="form-group">
                                @foreach ($errors->all() as $error)
                                    <div class="for-input help-block">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{ route('profile_save') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <div class="label"><label for="name" >Name</label></div>
                                <div class="for-input">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label"><label for="avatar" >Avatar</label></div>
                                <div class="for-input">
                                    <input id="avatar" type="file" class="form-control" name="avatar">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="for-input">
                                    <button type="submit" class="submit">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
