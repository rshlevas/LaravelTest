@extends('layouts.app')

@section('title', $user->name)

@section('content')
    <div class="container clearfix">
        <div class="row clearfix">
            <div class="content clearfix">
                <div class="profile-view">
                    <div class="avatar">
                        <img src="
                            @if ($avatar)
                                {{ asset('storage/images/' . $avatar->name) }}
                            @else
                                {{ asset('images/no_avatar.jpg') }}
                            @endif
                        ">
                    </div>
                    <div class="profile-info">
                        <p>Name: <strong>{{ $user->name }}</strong></p>
                        <p>Email: <strong>{{ $user->email }}</strong></p>
                        <p>Number of projects: <strong>{{ $user->projects->count()  }}</strong></p>
                        <p>Number of tasks: <strong>{{ $user->tasks->count() }}</strong></p>
                        <p>Tasks that need to be finished today: <strong>{{ $user->tasks()->term($date)->count() }}</strong></p>
                        <p>
                            <a href="{{ route('profile_update') }}">Edit</a>
                            <a href="{{ route('avatar_delete') }}">Avatar Del</a>
                        </p>
                    </div>
                 </div>
            </div>
        </div>
    </div>
@endsection