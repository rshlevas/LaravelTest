@extends('layouts.app')

@section('title', 'Task Creating...')

@section('content')
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="task-lits-body">
                    @if (!$projects->first())
                        <div class="form-title"><h3>You have to create project at first. Then you'll be able to add tasks!!</h3></div>
                        <div class="btn-back"><a href="{{ route('home') }}">Back</div>
                    @else
                        <div class="form-title"><h3>Task Creating</h3></div>

                        <div class="form-body">
                            @if ($errors->any())
                                <div class="form-group">
                                    @foreach ($errors->all() as $error)
                                        <div class="for-input help-block">{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif
                            <form class="form" method="POST" action="{{ route('task_save') }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <div class="label"><label for="name">Name</label></div>
                                    <div class="for-input">
                                        <input id="name" type="text" class="form-control" name="name"
                                               value="" placeholder="Enter task name" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="label"><label for="project_id">Project</label></div>
                                    <div class="for-input">
                                        <select id="project_id" name="project_id">
                                            @if (isset($project))
                                                @foreach($projects as $projectItem)
                                                    <option value="{{ $projectItem->id }}"
                                                            @if ($projectItem->id == $project->id)
                                                            selected
                                                            @endif
                                                    >{{ $projectItem->name }}</option>
                                                @endforeach
                                            @else
                                                @foreach($projects as $projectItem)
                                                    <option value="{{ $projectItem->id }}">{{ $projectItem->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="label"><label for="priority">Priority</label></div>
                                    <div class="for-input">
                                        <select id="priority" name="priority">
                                            <option value="0">Low</option>
                                            <option value="1">High</option>
                                            <option value="2">Very High</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="label"><label for="date">Date</label></div>
                                    <div class="for-input">
                                        <input id="date" type="date" name="date" value="{{ $date->toDateString() }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="for-input">
                                        <button type="submit" class="submit">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection