@extends('layouts.app')

@section('title', 'Task Updating...')

@section('content')
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="task-lits-body">
                    <div class="form-title"><h3>Task Updating</h3></div>

                    <div class="form-body">
                        @if ($errors->any())
                            <div class="form-group">
                                @foreach ($errors->all() as $error)
                                    <div class="for-input help-block">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif

                        <form class="form" method="POST" action="{{ route('task_update', ['id' => $task->id]) }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <div class="label"><label for="name" >Name</label></div>
                                <div class="for-input">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $task->name }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="label"><label for="project_id">Project</label></div>
                                <div class="for-input">
                                    <select id="project_id" name="project_id">
                                        @foreach($projects as $project)
                                            <option value="{{ $project->id }}"
                                                    @if ($project->id == $task->project_id)
                                                    selected
                                                    @endif
                                            >{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label"><label for="priority">Priority</label></div>
                                <div class="for-input">
                                    <select id="priority" name="priority">
                                        <option value="0"
                                                @if ($task->priority == 0)
                                                selected
                                                @endif
                                        >Low</option>
                                        <option value="1"
                                                @if ($task->priority == 1)
                                                selected
                                                @endif
                                        >High</option>
                                        <option value="2"
                                                @if ($task->priority == 2)
                                                selected
                                                @endif
                                        >Very High</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label"><label for="state">Status</label></div>
                                <div class="for-input">
                                    <select id="state" name="state">
                                        <option value="0"
                                                @if ($task->state == 0)
                                                selected
                                                @endif
                                        >In process</option>
                                        <option value="1"
                                                @if ($task->state == 1)
                                                selected
                                                @endif
                                        >Finished</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label"><label for="date">Date</label></div>
                                <div class="for-input">
                                    <input id="date" type="date" name="date" value="{{ $task->date }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="for-input">
                                    <button type="submit" class="submit">
                                        Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" value="{{ $task->id }}" name="task_id"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
