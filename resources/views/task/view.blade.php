@extends('layouts.app')

@section('title', 'Task №'.$task->id)

@section('content')
    <div class="container clearfix">
        <div class="row clearfix">
            <div class="content">
                <div class="task-view-container">
                    <div class="task-header"><h2>Task №{{ $task->id }}</h2></div>
                    <div class="task-lits-body">
                        <div class="task-item">
                            <div class="task-name"><a
                                        href="{{ route('task_view', ['id' => $task->id]) }}">{{  $task->name }}</a>
                            </div>
                            <div class="task-category">{{ $task->project->name }}</div>
                            <div class="task-priority
                                @php $date = \Carbon\Carbon::today() @endphp
                                @if ($task->date >= $date->toDateString())
                                    priority-{{ $task->priority }}
                                @endif
                                    ">
                            </div>

                            <div class="task-content">
                                <div class="task-date">Up to: {{ $task->date }}</div>
                                <div class="task-links">
                                    <a href="{{ route('task_update', ['id' => $task->id]) }}">Edit</a>
                                    <a href="{{ route('task_delete', ['id' => $task->id]) }}">Delete</a>
                                </div>

                            </div>
                        </div>
                        <div class="btn-back"><a href="{{ route('project_view', ['id' => $task->project_id]) }}">Back</div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection