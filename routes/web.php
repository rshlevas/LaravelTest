<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home', 301)->name('general');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

// Project

Route::middleware('projectRelations')->group(function () {
    Route::get('/project/{id}/', 'ProjectController@view')->where('id', '[0-9]+')->name('project_view');

    Route::get('/project/edit/{id}', 'ProjectController@update')->where('id', '[0-9]+')->name('project_update');

    Route::get('/project/delete/{id}', 'ProjectController@delete')->where('id', '[0-9]+')->name('project_delete');
});

Route::get('/project/create', 'ProjectController@create')->name('project_create');

Route::post('/project/save', 'ProjectController@store')->name('project_save');

// Routes for TaskController
Route::middleware('taskRelations')->group(function () {
    Route::match(['get', 'post'], '/task/edit/{id}', 'TaskController@update')->where('id', '[0-9]+')->name('task_update');

    Route::get('/task/delete/{id}', 'TaskController@delete')->where('id', '[0-9]+')->name('task_delete');

    Route::get('/task/{id}', 'TaskController@view')->where('id', '[0-9]+')->name('task_view');
});

Route::post('/task/save', 'TaskController@store')->name('task_save');



Route::get('/task/create', 'TaskController@create')->name('task_create');



// Archive

Route::get('/archive', 'ArchiveController@view')->name('archive_view');

//Profile

Route::get('/profile', 'ProfileController@view')->name('profile_view');

Route::get('/profile/edit', 'ProfileController@update')->name('profile_update');

Route::post('/profile/save', 'ProfileController@store')->name('profile_save');

Route::get('/profile/avatar/delete', 'ProfileController@avatarDelete')->name('avatar_delete');

//Test
Route::get('/try', 'TestController@index');