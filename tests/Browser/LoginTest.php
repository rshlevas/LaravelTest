<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use Tests\Traits\GetDBObjects;

class LoginTest extends DuskTestCase
{

    use GetDBObjects;
    /**
     * Test that check ability of not authorized users get to the
     * site resourses.
     * As result they have to be redirected to the login page
     *
     *
     * @group authorization
     * @return void
     */
    public function testNotAuthorized()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/home')
                    ->assertPathIs('/login')
                    ->visit(route('profile_view'))
                    ->assertPathIs('/login')
                    ->visit(route('project_create'))
                    ->assertPathIs('/login');
        });
    }

    /**
     *
     * Test that checked login function of the site, and ability
     * to get to profile page after authorization.
     *
     *
     * @group login
     * @return void
     */

    public function testLogin()
    {
        $user =$this->getUser();
        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', '111111')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee($user->name)
                ->visit(route('profile_view'))
                ->assertPathIs('/profile')
                ->assertSee('Name: ' . $user->name);
        });

        $user->delete();
    }


    /**
     *
     * Test that checked registration function of the site
     *
     *
     *
     * @group registration
     * @return void
     */

    public function testRegistration()
    {
        $this->browse(function ($first, $second) {
            $first->visit('/register')
                ->type('name', 'Joshua')
                ->type('email', 'you@you.com')
                ->type('password', 'secret')
                ->type('password_confirmation', 'secret')
                ->press('Register')
                ->assertPathIs('/home')
                ->assertSee('no tasks in this project');

            $second->visit('/register')
                ->type('name', 'Joshua')
                ->type('email', 'you@you.com')
                ->type('password', 'secret')
                ->type('password_confirmation', 'secret')
                ->press('Register')
                ->assertPathIs('/register')
                ->assertSee('The email has already been taken');
        });

        $user = User::where('name', 'joshua')->get();
        $user[0]->delete();
    }
}
