<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Traits\GetDBObjects;


class ProjectCreateTest extends DuskTestCase
{

    use GetDBObjects;
    /**
     * User can't create project with the same name
     *
     * @group projectValidation
     * @return void
     */
    public function testUserCantCreateProjectWithTheSameName()
    {
        $user = $this->getUser();
        $projectName = 'StarWars';
        $project = $this->getProject($user->id, ['name' => $projectName]);
        $this->browse(function (Browser $browser) use ($user, $projectName){

            $browser->loginAs($user)
                ->visit('/home')
                ->clickLink('<- Add Project')
                ->assertPathIs('/project/create')
                ->type('name', $projectName)
                ->press('Create')
                ->assertPathIs('/project/create')
                ->assertSee('The name has already been taken.')
                ->screenshot('Create');
        });

        $user->delete();
        $project->delete();
    }

    /**
     *
     *
     * @group projectValidation
     *
     */
    public function testDifferentUsersCanCreateSimilarProjects()
    {
        $users = $this->getSeveralUsers(3);
        $john = $users[0];
        $bob = $users[1];
        $simon = $users[2];
        $projectName = 'StarWars';

        $this->browse(function (Browser $johnAction, $bobAction, $simonAction) use ($john, $bob, $simon, $projectName){
            $johnAction->loginAs($john)
                ->visit(route('home'))
                ->assertSee('<- Add Project')
                ->clickLink('<- Add Project')
                ->assertPathIs('/project/create')
                ->type('name', $projectName)
                ->press('Create')
                ->assertSee($projectName);

            $bobAction->loginAs($bob)
                ->visit(route('home'))
                ->assertSee('<- Add Project')
                ->clickLink('<- Add Project')
                ->assertPathIs('/project/create')
                ->type('name', $projectName)
                ->press('Create')
                ->assertSee($projectName);

            $simonAction->loginAs($simon)
                ->visit(route('home'))
                ->assertSee('<- Add Project')
                ->clickLink('<- Add Project')
                ->assertPathIs('/project/create')
                ->type('name', $projectName)
                ->press('Create')
                ->assertSee($projectName);
        });

        foreach ($users as $user)
            $user->delete();
    }
}