<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Traits\GetDBObjects;


class ProjectUpdateTest extends DuskTestCase
{

    use GetDBObjects;
    /**
     * Test that check ability to update the project name
     *
     * @group project
     * @return void
     */
    public function testProjectUpdate()
    {
        $user = $this->getUser();
        $projectFirst = $this->getProject($user->id);
        $projectSecond = $this->getProject($user->id);
        $this->browse(function (Browser $update, $dontUpdate) use ($user, $projectFirst, $projectSecond){

            $update->loginAs($user)
                ->visit('/home')
                ->assertSee($projectFirst->name)
                ->assertSee($projectSecond->name)
                ->visit(route('project_update', ['id' => $projectFirst->id]))
                ->assertSee('Project Updating')
                ->type('name', 'ProjectUpdate')
                ->press('Update')
                ->assertPathIs('/project/' . $projectFirst->id)
                ->assertSee('ProjectUpdate');

            $dontUpdate->loginAs($user)
                ->visit('/home')
                ->assertSee('ProjectUpdate')
                ->assertSee($projectSecond->name)
                ->visit('/project/edit/' . $projectFirst->id)
                ->assertSee('Project Updating')
                ->type('name', $projectSecond->name)
                ->press('Update')
                ->assertPathIs('/project/edit/' . $projectFirst->id)
                ->assertSee('The name has already been taken.');
        });

        $user->delete();
        $projectSecond->delete();
        $projectFirst->delete();
    }
}