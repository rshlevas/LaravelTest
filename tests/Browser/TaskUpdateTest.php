<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Traits\GetDBObjects;

class TaskUpdateTest extends DuskTestCase
{
    use GetDBObjects;
    /**
     * Test that check ability to update the task name
     *
     * @group taskUpdate
     * @return void
     */
    public function testUpdatingTaskByRelatedUser()
    {
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $task = $this->getTask($user->id, $project->id, ['name' => 'TestTask']);

        $this->browse(function (Browser $browser) use ($user, $task){
            $browser->loginAs($user)
                ->visit('/home')
                ->assertSee($task->name)
                ->visit(route('task_update', ['id' => $task->id]))
                ->assertSee('Task Updating')
                ->type('name', 'TaskUpdate')
                ->keys('#date', '10-12-2017')
                ->press('Update')
                ->assertPathIs('/project/' . $task->project->id)
                ->assertSee('TaskUpdate');
        });

        $user->delete();
        $project->delete();
        $task->delete();
    }

    /**
     * Test that check ability to update the task name
     *
     * @group taskUpdate
     * @return void
     */
    public function testUpdatingTaskByNotRelatedUser()
    {
        $users = $this->getSeveralUsers(2);
        $owner = $users[0];
        $notOwner = $users[1];
        $project = $this->getProject($owner->id);
        $task = $this->getTask($owner->id, $project->id, ['name' => 'TestTask']);

        $this->browse(function (Browser $browser) use ($notOwner, $task){
            $browser->loginAs($notOwner)
                ->visit('/home')
                ->assertDontSee($task->name)
                ->visit(route('task_update', ['id' => $task->id]))
                ->assertPathIs('/home');

        });

        foreach($users as $user)
            $user->delete();
        $project->delete();
        $task->delete();
    }

    /**
     *
     *
     * @group taskValidation
     *
     */
    public function testUserCantCreateSameTasks()
    {
        $john = $this->getUser();
        $johnProject = $this->getProject($john->id);
        $task = $this->getTask($john->id, $johnProject->id, ['name' => 'JohnTask']);

        $this->browse(function (Browser $browser) use ($john, $task){
            $browser->loginAs($john)
                ->visit(route('home'))
                ->assertSee('Add Task')
                ->clickLink('Add Task')
                ->assertPathIs('/task/create')
                ->assertSee('Task Creating')
                ->type('name', $task->name)
                ->press('Create')
                ->assertPathIs('/task/create')
                ->assertSee('The name has already been taken.');
        });

        $john->delete();
        $johnProject->delete();
        $task->delete();
    }

    /**
     *
     *
     * @group taskValidation
     *
     */
    public function testUserCanCreateSameTasksInDifferentProjects()
    {
        $john = $this->getUser();
        $johnProject = $this->getProject($john->id);
        $johnProject2 = $this->getProject($john->id);
        $task = $this->getTask($john->id, $johnProject->id, ['name' => 'JohnTask']);

        $this->browse(function (Browser $browser) use ($john, $task, $johnProject2){
            $browser->loginAs($john)
                ->visit(route('project_view', ['id' =>$johnProject2->id]))
                ->assertSee('Add Task')
                ->clickLink('Add Task')
                ->assertSee('Task Creating')
                ->type('name', $task->name)
                ->press('Create')
                ->assertPathIs('/project/' . $johnProject2->id)
                ->assertSee($task->name);
        });

        $john->delete();
        $johnProject->delete();
        $johnProject2->delete();
        $task->delete();
    }
}