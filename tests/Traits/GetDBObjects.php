<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.10.17
 * Time: 12:25
 */

namespace Tests\Traits;

use App\User;
use App\Project;
use App\Task;


trait GetDBObjects
{
    /**
     * @return mixed
     */
    protected function getUser(array $options = null)
    {
        if (!$options) {
            return factory(User::class)->create();
        }

        return factory(User::class)->create($options);
    }

    /**
     * @param $userId
     * @return mixed
     */
    protected function getProject($userId, array $options = null)
    {
        $options['user_id'] = $userId;
        return factory(Project::class)->create($options);
    }

    /**
     * @param count
     * @return mixed
     */
    protected function getSeveralUsers($count)
    {
        return factory(User::class, $count)->create();
    }

    /**
     * @param $userId
     * @param $projectId
     * @return mixed
     */
    protected function getTask($userId, $projectId, array $options = null)
    {
        $options['user_id'] = $userId;
        $options['project_id'] = $projectId;
        return factory(Task::class)->create($options);
    }

    protected function getSeveralTasks(int $userId, int $projectId, int $count) : array
    {
        $tasks = [];
        for($i = 0; $i < $count; $i++) {
            $tasks[] = $this->getTask($userId, $projectId);
        }
        return $tasks;
    }
}