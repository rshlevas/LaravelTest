<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.10.17
 * Time: 8:38
 */

namespace Tests\Traits;

use App\User;
use App\Project;
use App\Task;

trait GetMockObjects
{

    /**
     * Create mock user
     *
     * @return \Mockery\MockInterface
     */
    private function getMockUser()
    {
        return \Mockery::mock(User::class);
    }

    /**
     * Create mock user with related to him task and project objects
     *
     * @param int $projectCount
     * @param int $taskCount
     * @return \Mockery\MockInterface
     */
    private function getMockUserWithProjectsAndTasks(int $projectCount, int $taskCount)
    {
        $user = $this->getMockUser();
        $user->shouldReceive('getAttribute')
            ->with('projects')
            ->andReturn($this->getMockProjects($projectCount));
        $user->shouldReceive('getAttribute')
            ->with('tasks')
            ->andReturn($this->getMockTasks($taskCount));
        return $user;
    }

    /**
     * Create array of mocked project objects
     *
     * @param int $count
     * @return array
     */
    private function getMockProjects(int $count)
    {
        $projects = [];
        for ($i = 0; $i < $count; $i++) {
            $projects[] = $this->getMockProject($i + 1);
        }
        return $projects;
    }


    /**
     * Create mocked project
     *
     * @param int $id
     * @return \Mockery\MockInterface
     */
    private function getMockProject(int $id)
    {
        $project = \Mockery::mock(Project::class);
        $project->shouldReceive('getAttribute')
            ->with('id')
            ->andReturn($id);
        return $project;
    }

    /**
     * Create array with mocked task objects
     *
     * @param int $count
     * @return array
     */
    private function getMockTasks(int $count)
    {
        $tasks = [];
        for ($i = 0; $i < $count; $i++) {
            $tasks[] = $this->getMockTask($i + 1);
        }
        return $tasks;
    }

    /**
     * Create mocked task object with some id
     *
     * @param int $id
     * @return \Mockery\MockInterface
     */
    private function getMockTask(int $id)
    {
        $task = \Mockery::mock(Task::class);
        $task->shouldReceive('getAttribute')
            ->with('id')
            ->andReturn($id);
        return $task;
    }

}