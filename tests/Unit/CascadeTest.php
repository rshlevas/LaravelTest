<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Task;
use App\Project;
use App\User;
use Tests\Traits\GetDBObjects;

class CascadeTest extends TestCase
{
    use RefreshDatabase, GetDBObjects;

    protected $user;

    protected $task;

    protected $project;

    /**
     *
     */
    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->getUser();
        $this->project = $this->getProject($this->user->id);
        $this->task = $this->getTask($this->user->id, $this->project->id);
    }

    /**
     *
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->user = null;
        $this->project = null;
        $this->task = null;
    }

    /**
     * Test which check cascade deleting of
     * tasks related to the project
     *
     * @group cascade
     * @return void
     */
    public function testCascadeTaskDeleting()
    {
        // Looking for project in DB
        $projectCopy = Project::find($this->project->id);

        // Taking related task
        $relatedTask = $projectCopy->tasks->first();

        // it should be equal to task that we created
        $this->assertEquals($this->task->id, $relatedTask->id);

        /*
         * When we delete the project from DB, we can't find there
         * this task anymore
         */
        $projectCopy->delete();
        $TaskTry = Task::find($relatedTask->id);
        $this->assertNull($TaskTry);
    }
}
