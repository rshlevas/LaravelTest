<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.10.17
 * Time: 9:42
 */

namespace Tests\Unit;

use App\Services\FilterService;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\GetDBObjects;
use Tests\Traits\GetMockObjects;


class FilterServiceRelationsTest extends TestCase
{
    use RefreshDatabase, GetDBObjects, GetMockObjects;

    private $_FilterService;

    public function setUp()
    {
        parent::setUp();
        $this->_FilterService = new FilterService();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->_FilterService = null;
    }

    /**
     * Test that checked FilterService for
     * ability to check related task of user
     *
     * @group FilterServiceUserRelations
     * return void
     */
    public function testUserIsRelatedToTask()
    {
        //Create environement
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $task = $this->getTask($user->id, $project->id);
        $type = 'task';

        //Testing if task is related to user
        $this->actingAs($user);
        $result = $this->_FilterService->userRelationsChecker($task->id, $type);
        $this->assertTrue($result, 'Showed, that task not related to user');
    }

    /**
     * Test that checked FilterService for
     * ability to check related task of user
     * if non existed task given
     *
     * @group FilterServiceUserRelations
     * return void
     */
    public function testUserIsRelatedToNonExistedTask()
    {
        //Create environement
        $user = $this->getUser();
        $type = 'task';

        //Testing if task is related to user
        $this->actingAs($user);
        $result = $this->_FilterService->userRelationsChecker(1000, $type, $user);
        $this->assertFalse($result, 'Showed, that task is exist and related to user');
    }

    /**
     * Test that checked FilterService for
     * ability to check related project of user
     *
     * @group FilterServiceUserRelations
     * return void
     */
    public function testUserIsRelatedToProject()
    {
        //Create environement
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $type = 'project';

        //Testing if task is related to user
        $this->actingAs($user);
        $result = $this->_FilterService->userRelationsChecker($project->id, $type, $user);
        $this->assertTrue($result, 'Showed, that project not related to user');
    }

    /**
     * Test that checked FilterService for
     * ability to check related project of user
     * if non existed project given
     *
     * @group FilterServiceUserRelations
     * return void
     */
    public function testUserIsRelatedToNonExistProject()
    {
        //Create environement
        $user = $this->getUser();
        $type = 'project';

        //Testing if task is related to user
        $this->actingAs($user);
        $result = $this->_FilterService->userRelationsChecker(1000, $type, $user);
        $this->assertFalse($result, 'Showed, that project exist and related to user');
    }

    /**
     * Test that checked FilterService if
     * non-existing type of object given
     *
     * @group FilterServiceUserRelations
     * return void
     */
    public function testUserIsRelatedWithFalseObjectType()
    {
        //Create environement
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $task = $this->getTask($user->id, $project->id);
        $type = 'non-exist';

        //Testing if task is related to user
        $this->actingAs($user);
        $resultForTaskId = $this->_FilterService->userRelationsChecker($task->id, $type, $user);
        $this->assertFalse($resultForTaskId, 'Showed, that non-exist object is task type');
        $resultForProjectId = $this->_FilterService->userRelationsChecker($project->id, $type, $user);
        $this->assertFalse($resultForProjectId, 'Showed, that non-exist object is project type');
    }

    /**
     * Test that checked FilterService for
     * ability to check related project of user
     *
     * @dataProvider typesProvider
     * @group Try
     * return void
     */
    public function testUserIsRelatedToObject($id, $type)
    {
        //Create environement

        $user = $this->getMockUserWithProjectsAndTasks(1, 1);

        //Testing if task is related to user

        $result = $this->_FilterService->userRelationsChecker($id, $type, $user);
        $this->assertTrue($result, "Object($type) with id($id) not related to user");

    }


    /**
     * Test that checked FilterService in conditions of
     * incorrect type attribute
     *
     * @group Try
     * return void
     */
    public function testForIncorrectTypeAttribute()
    {
        //Create environement
        $user = \Mockery::mock(User::class);
        $type = 'incorrect';

        //Testing
        $this->expectException(\Exception::class);
        $this->_FilterService->userRelationsChecker(1, $type, $user);

    }

    /**
     * Provider for testUserIsRelatedToObject
     *
     * @return array
     */
    public function typesProvider()
    {
        $types = [
            [1, 'projects'],
            [1, 'tasks'],
            [2, 'projects'],
        ];

        return $types;
    }
}