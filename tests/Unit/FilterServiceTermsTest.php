<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.10.17
 * Time: 16:47
 */

namespace Tests\Unit;

use App\Services\FilterService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\GetDBObjects;


class FilterServiceTermsTest extends TestCase
{
    use RefreshDatabase, GetDBObjects;

    private $_filterService;

    public function setUp()
    {
        parent::setUp();
        $this->_filterService = new FilterService();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->_filterService = null;
    }

    /**
     *
     * @group filterTerm
     * @return void
     */
    public function testNotValidTerm()
    {
        $term = 'not-valid';
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $task = $this->getTask($user->id, $project->id);
        $this->actingAs($user);
        $result = $this->_filterService->getTasksByTerm($project, $term);
        $this->assertEmpty($result);
    }

    /**
     *
     * @group filterTerm
     * @return void
     */
    public function testNotValidObject()
    {
        $term = "week";
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $task = $this->getTask($user->id, $project->id);
        $this->actingAs($user);
        $result = $this->_filterService->getTasksByTerm($task, $term);
        $this->assertEmpty($result);
    }

    /**
     *
     * @group allfilterTerm
     * @return void
     */
    public function testNotValidCollection()
    {
        $term = null;
        $userCollection = $this->getSeveralUsers(3);
        $this->actingAs($userCollection->first());
        $result = $this->_filterService->getTasksByTerm($userCollection, $term);
        $this->assertEmpty($result);
    }
}