<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Task;
use App\Project;
use App\User;
use Tests\Traits\GetDBObjects;

class PrivacyTest extends TestCase
{
    use RefreshDatabase, GetDBObjects;

    /**
     * Test which check ability of user
     * get to interact with other users' projects
     *
     * @group privacy
     * @return void
     */
    public function testProjectPrivacy()
    {
        // Create user and project related to him
        $user = $this->getUser('you@you.you');
        $project = $this->getProject($user->id);

        // Create another user
        $anotherUser = $this->getUser('another@you.you');

        // User couldn't see update page of project,or delete it, if it doesn't belong to him
        $response = $this->actingAs($anotherUser)->get(route('project_update', ['id' => $project->id]));
        $response->assertStatus(302, 'Other user can see project update page');
        $response = $this->actingAs($anotherUser)->get(route('project_delete', ['id' => $project->id]));
        $response->assertStatus(302, 'Problem with redirecting');
        $projectAttemp = Project::find($project->id);
        $this->assertEquals($projectAttemp->id, $project->id);

        // User could see update page of his project and could delete it
        $response = $this->actingAs($user)->get(route('project_update', ['id' => $project->id]));
        $response->assertStatus(200, 'User can\'t see his project update page');
        $response = $this->actingAs($user)->get(route('project_delete', ['id' => $project->id]));
        $response->assertStatus(302, 'Problem with redirecting');
        $projectAttemp = Project::find($project->id);
        $this->assertNull($projectAttemp, 'User do not delete his project');
    }

    /**
     * Test which check ability of user
     * get to update pages of other users' tasks
     *
     * @group privacy
     * @return void
     */
    public function testTaskPrivacy()
    {
        // Create user and task related to him
        $user = $this->getUser();
        $project = $this->getProject($user->id);
        $task = $this->getTask($user->id, $project->id);

        // Create another user
        $anotherUser = $this->getUser();

        // User couldn't see update page of task,or delete it, if it doesn't belong to him
        $response = $this->actingAs($anotherUser)->get(route('task_update', ['id' => $task->id]));
        $response->assertStatus(302);
        $response = $this->actingAs($anotherUser)->get(route('task_delete', ['id' => $task->id]));
        $response->assertStatus(302);
        $TaskAttemp = Task::find($task->id);
        $this->assertEquals($TaskAttemp->id, $task->id);

        // User could see update page of his project and could delete it
        $response = $this->actingAs($user)->get(route('task_update', ['id' => $task->id]));
        $response->assertStatus(200, 'User can\'t see his project update page');
        $response = $this->actingAs($user)->get(route('task_delete', ['id' => $task->id]));
        $response->assertStatus(302);
        $TaskAttemp = Task::find($task->id);
        $this->assertNull($TaskAttemp, 'User can\'t deletehis task');
    }

}
